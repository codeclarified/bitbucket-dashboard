<?php
/*
 * login_with_bitbucket.php
 *
 * @(#) $Id: login_with_bitbucket.php,v 1.1 2012/12/19 11:01:44 mlemos Exp $
 *
 */
	//Simple config file
	// TODO: Create template config.php
	require('config.php');
	require('http.php');
	require('oauth_client.php');
	include('functions.php');

	$endpoint = $_POST['endpoint'];

	$client = new oauth_client_class;
	$client->debug = true;
	$client->debug_http = true;
	$client->access_token = $_POST['access_token'];
	$client->access_token_secret = $_POST['access_token_secret'];
	$client->server = 'Bitbucket';
	$client->redirect_uri = 'http://'.$_SERVER['HTTP_HOST'].
		dirname(strtok($_SERVER['REQUEST_URI'],'?')).'/login_with_bitbucket.php';

	$client->client_id = CONSUMER_KEY; $application_line = __LINE__;
	$client->client_secret = CONSUMER_SECRET;

	if(strlen($client->client_id) == 0
	|| strlen($client->client_secret) == 0)
		die('Please go to Bitbucket page to Manage Account '.
			'https://bitbucket.org/account/ , click on Integrated Applications, '.
			'then Add Consumer, and in the line '.$application_line.
			' set the client_id with Key and client_secret with Secret. '.
			'The URL must be '.$client->redirect_uri);

	if(($success = $client->Initialize()))
	{
		if(($success = $client->Process()))
		{
			if(strlen($client->access_token))
			{
				$success = $client->CallAPI(
					$endpoint, 
					'GET', array(), array('FailOnAccessError'=>true), $data);
			}
		}
		$success = $client->Finalize($success);
	}
	if($client->exit)
		exit;
	if($success)
	{

		$data_json = json_encode($data);
		echo $data_json;

	} else {
		$client_json = json_encode($client);
		echo $client_json;
	}
