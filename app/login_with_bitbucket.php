<?php
/*
 * login_with_bitbucket.php
 *
 * @(#) $Id: login_with_bitbucket.php,v 1.1 2012/12/19 11:01:44 mlemos Exp $
 *
 */
	//Simple config file
	// TODO: Create template config.php
	require('config.php');
	require('http.php');
	require('oauth_client.php');

	$client = new oauth_client_class;
	$client->debug = true;
	$client->debug_http = true;
	$client->server = 'Bitbucket';
	$client->redirect_uri = 'http://'.$_SERVER['HTTP_HOST'].
		dirname(strtok($_SERVER['REQUEST_URI'],'?')).'/login_with_bitbucket.php';

	$client->client_id = CONSUMER_KEY; $application_line = __LINE__;
	$client->client_secret = CONSUMER_SECRET;

	if(strlen($client->client_id) == 0
	|| strlen($client->client_secret) == 0)
		die('Please go to Bitbucket page to Manage Account '.
			'https://bitbucket.org/account/ , click on Integrated Applications, '.
			'then Add Consumer, and in the line '.$application_line.
			' set the client_id with Key and client_secret with Secret. '.
			'The URL must be '.$client->redirect_uri);

	if(($success = $client->Initialize()))
	{
		if(($success = $client->Process()))
		{
			if(strlen($client->access_token))
			{
				$success = $client->CallAPI(
					'https://api.bitbucket.org/1.0/user', 
					'GET', array(), array('FailOnAccessError'=>true), $user);
			}
		}
		$success = $client->Finalize($success);
	}
	if($client->exit)
		exit;
	if($success)
	{

	$client_json = json_encode($client);
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>Bitbucket OAuth client results</title>
</head>
<body>
<?php
		echo 'Logged in, redirecting...';

?>
<script>
sessionStorage.setItem("access_token", '<?php echo $client->access_token; ?>');
sessionStorage.setItem("access_token_secret", '<?php echo $client->access_token_secret; ?>');
sessionStorage.setItem("client", JSON.stringify(<?php echo $client_json; ?>));
var loc = window.location,
    newloc = "<?php echo 'http://'.$_SERVER['HTTP_HOST'].dirname(strtok($_SERVER['REQUEST_URI'],'?')); ?>";
window.location = newloc;
</script>
</body>
</html>
<?php
	}
	else
	{
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>OAuth client error</title>
</head>
<body>
<h1>OAuth client error</h1>
<pre>Error: <?php echo HtmlSpecialChars($client->error); ?></pre>
</body>
</html>
<?php
	}

?>