require.config({
    paths: {
        jquery: '../components/jquery/jquery',
        bootstrap: 'vendor/bootstrap'
    },
    shim: {
        bootstrap: {
            deps: ['jquery'],
            exports: 'jquery'
        }
    }
});

require(['app', 'jquery', 'bootstrap'], function (app, $) {
    'use strict';
jQuery(document).ready(function($){

    var access_token = sessionStorage.getItem("access_token"),
        access_token_secret = sessionStorage.getItem("access_token_secret"),
        profile = JSON.parse(sessionStorage.getItem("user"));

    if(!access_token){
        console.log(window.location);
        var loc = window.location,
            newloc = loc.protocol + '//' + loc.host + loc.pathname + 'login_with_bitbucket.php';
        window.location = newloc;
        //window.location = loc.protocol + '//' + loc.host + loc.pathname + '/login_with_bitbucket.php';
        //window.location = '/login_with_bitbucket.php';
    }



        console.log(profile);
    //Grab user's profile and place in local storage to save calls & speed things up
    if(!profile){
        var request_url = 'https://api.bitbucket.org/1.0/user';
        $.ajax({
            url: 'tunnel.php',
            data: {
                access_token: access_token,
                access_token_secret: access_token_secret,
                endpoint: request_url
            },
            dataType: 'json',
            type: 'POST',
            success: function(d,s){
                sessionStorage.setItem("user", JSON.stringify(d.user));

                profile = d.user;
                printProfile(profile);
                console.log(d);
            }
        });
    } else {
        printProfile(profile);
    }
    $('#load-repos').click(function(e){
        e.preventDefault();

        var request_url = 'https://api.bitbucket.org/1.0/user/repositories';

        $.ajax({
            url: 'tunnel.php',
            data: {
                access_token: access_token,
                access_token_secret: access_token_secret,
                endpoint: request_url
            },
            dataType: 'json',
            type: 'POST',
            success: function(d,s){
                console.log('Status: ',s);
                console.log('Data: ',d);
            }
        });
    });

    $('a[data-toggle="tab"]').on('show', function (e) {
        var atab = $(e.target),
            ahref = atab.attr('href'),
            apane = $(ahref);

        if(ahref === '#repos' && $('#repo-list').children().length < 1){
            printRepos();
        }

    });

    function printRepos(){
        var request_url = 'https://api.bitbucket.org/1.0/user/repositories';
        $.ajax({
            url: 'tunnel.php',
            data: {
                access_token: access_token,
                access_token_secret: access_token_secret,
                endpoint: request_url
            },
            dataType: 'json',
            type: 'POST',
            success: function(d,s){
                if(d.response_status === 401){
                    sessionStorage.removeItem('access_token');
                    sessionStorage.removeItem('access_token_secret');
                    sessionStorage.removeItem('client');
                    sessionStorage.removeItem('user');
                    console.log('Auth failed, trying again');
                    printRepos();
                } else {

                    $.each(d, function(i,v){
                        console.log(v);
                        var output = $('<li class="media" data-owner="' + v.owner + '" data-wiki="' + v.has_wiki + '" data-repo="' + v.slug + '" />');

                        output.append('<a class="pull-left" href="' + v.website + '"><img class="media-object img-circle" src="' + v.logo + '" /></a>');
                        output.append('<div class="media-body" />');
                        var media_body = output.find('.media-body');
                        media_body.append('<h4 class="media-heading" data-repo="' + v.slug + '">' + v.name + '</h4>');
                        media_body.append('<p>' + v.description + '</p>');

                        if(v.is_private === true){
                            media_body.find('h4').append(' <span class="badge badge-important">Private</span>');
                        }

                        if(v.has_issues === true){
                            var load_button = $('<a class="btn btn-info">View Issues</a>')
                                                .addClass('load-issues')
                                                .data('repo', v.slug)
                                                .data('offset', 0)
                                                .appendTo(media_body);
                        }

                        output.hide().appendTo('#repo-list').fadeIn();
                    });
                }

            }
        });
    }

    function printProfile(user){
        var wrap = $('#profile');
        wrap.prepend('<h2><small>' + user.display_name + '</small></h2>');
        wrap.find('h2').prepend('<img class="img-circle" src="' + user.avatar + '" /> ');

    }
    
    function statusDropdown(current){

        var states = new Array(
            'open',
            'new',
            'resolved',
            'on hold',
            'invalid',
            'duplicate',
            'wontfix'
        ),
        output = '<select name="status">';
        $.each(states, function(i,v){

            output += '<option value="' + v + '"';
            if(current === v){
                output += ' selected="selected"';
            }
            output += '>' + v + '</option>';
        });
        output += '</select>';
        return output;
    }

$('.container-fluid').on('click', '.media-heading', function(e){
    e.preventDefault();
    if($(this).data('wiki') == "false"){
        return false;
    }
    var t = $(this),
        wrap = t.parents('.media'),
        request_url = 'https://api.bitbucket.org/1.0/repositories/' + wrap.data('owner') + '/' + t.data('repo') + '/wiki/';
 
    $.ajax({
        url: 'tunnel.php',
        data: {
            access_token: sessionStorage.getItem("access_token"),
            access_token_secret: sessionStorage.getItem("access_token_secret"),
            endpoint: request_url
        },
        dataType: 'json',
        type: 'POST',
        success: function(d){
            console.log(d);
        }
    });

}).on('click', '.load-issues', function(e){
    e.preventDefault();
    var t = $(this),
        wrap = t.parents('.media'),
        request_url = 'https://api.bitbucket.org/1.0/repositories/' + wrap.data('owner') + '/' + t.data('repo') + '/issues?status=new&status=open&limit=50&start=' + t.data('offset');
    console.log(request_url);
    $.ajax({
        url: 'tunnel.php',
        data: {
            access_token: sessionStorage.getItem("access_token"),
            access_token_secret: sessionStorage.getItem("access_token_secret"),
            endpoint: request_url
        },
        dataType: 'json',
        type: 'POST',
        beforeSend: function(){
            t.addClass('disabled');
        },
        success: function(d){

            var accordion_count = $('.accordion').length,
                output = '<div class="accordion" id="accordion' + accordion_count + '">';

            $.each(d.issues, function(i,v){
               output +=  '<div data-id="' + v.local_id + '" class="accordion-group">' +
                            '<div class="accordion-heading">';
                output += '<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion' + accordion_count + '" href="#collapse' + accordion_count + '_' + i + '">';
                output += v.title;
                output += '</a>' +
                        '</div>' +
                        '<div id="collapse' + accordion_count + '_' + i + '" class="accordion-body collapse">' +
                            '<div class="accordion-inner">' +
                                '<h4><small>Created by: ' + v.reported_by.display_name + '</small></h4>' +
                                '<ul>' +
                                    '<li>Type: ' + v.metadata.kind + '</li>' +
                                    '<li>Priority: ' + v.priority + '</li>' +
                                    '<li>Status: ' + statusDropdown(v.status) + '</li>' +
                                '</ul>' +
                                '<p>' +
                                    v.content +
                                '</p>' +
                            '</div>' +
                        '</div>' +
                    '</div>';
            });
            output += '</div>';
            if(t.data('offset') + 50 > d.count){
                t.before(output).fadeOut();
            } else {
                t.before(output).data('offset', t.data('offset') + 50).html('Load more...').removeClass('disabled');
            }
        }
    });
}).on('change', '[name="status"]', function(){
}).on('show', '.accordion-group[data-id]', function(){
    var t = $(this),
        wrap = t.parents('.media'),
        request_url = 'https://api.bitbucket.org/1.0/repositories/' + wrap.data('owner') + '/' + wrap.data('repo') + '/issues/' + t.data('id') + '/comments';
    $.ajax({
        url: 'tunnel.php',
        data: {
            access_token: sessionStorage.getItem("access_token"),
            access_token_secret: sessionStorage.getItem("access_token_secret"),
            endpoint: request_url
        },
        dataType: 'json',
        type: 'POST',
        beforeSend: function(){
            t.addClass('disabled');
        },
        success: function(d){
            console.log(d);
            var output = '<ul class="issue-comments media-list">';

            $.each(d, function(i,v){
                output += '<li class="media">' +
                          '<a class="pull-left">' +
                            '<img class="media-object" src="' + v.author_info.avatar + '" />' +
                          '</a>' +
                        '<div class="media-body">' +
                          '<h4 class="media-heading">' + v.author_info.display_name + '</h4>' +
                          '<p>' + v.content + '</p>' +
                        '</div>' +
                      '</li>';
            });
            output += '</ul>';
            t.find('.accordion-body').append(output);
        }
    });
});

});
});